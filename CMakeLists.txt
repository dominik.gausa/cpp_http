cmake_minimum_required (VERSION 3.8)


enable_language(CXX)


set(projname server)
find_package(Threads REQUIRED)

file(GLOB_RECURSE SOURCES_${projname}_H "${CMAKE_CURRENT_LIST_DIR}/src/*.h")
file(GLOB_RECURSE SOURCES_${projname}_C "${CMAKE_CURRENT_LIST_DIR}/src/*.c")
file(GLOB_RECURSE SOURCES_${projname}_CXX "${CMAKE_CURRENT_LIST_DIR}/src/*.cpp")


set(SOURCES_${projname}
    ${SOURCES_${projname}_H}
    ${SOURCES_${projname}_C}
    ${SOURCES_${projname}_CXX})

source_group(
    TREE ${CMAKE_CURRENT_LIST_DIR} 
    FILES ${SOURCES_${projname}}
    )

project(${projname})
add_executable(${projname} ${SOURCES_${projname}})
target_link_libraries(${projname} -lpthread ${PTHREAD_LIB})

target_include_directories(${projname} PRIVATE 
  "${CMAKE_CURRENT_LIST_DIR}/src"
  "${CMAKE_CURRENT_LIST_DIR}/src/Common")
