#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include <sstream>

#include "Client.h"


Client::Client(std::string serverName, std::string url, int port) :
  serverName(serverName), port(port), url(url) {

}

std::string Client::request(){

	struct sockaddr_in server_address;
	memset(&server_address, 0, sizeof(server_address));
	server_address.sin_family = AF_INET;

	// creates binary representation of server name
	// and stores it as sin_addr
	// http://beej.us/guide/bgnet/output/html/multipage/inet_ntopman.html
	inet_pton(AF_INET, serverName.c_str(), &server_address.sin_addr);

	// htons: port in network order format
	server_address.sin_port = htons(port);

	// open a stream socket
	int sock;
	if ((sock = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		printf("could not create socket\n");
		return "";
	}

	// TCP is connection oriented, a reliable connection
	// **must** be established before any data is exchanged
	if (connect(sock, (struct sockaddr*)&server_address,
	            sizeof(server_address)) < 0) {
		printf("could not connect to server\n");
		return "";
	}

	// send

	// data that will be sent to the server
  std::ostringstream httpRequest;
  httpRequest << "GET " << this->url << " HTTP/1.1\r\n";
  httpRequest << "\r\n";

	send(sock, httpRequest.str().c_str(), httpRequest.str().length(), 0);


	// will remain open until the server terminates the connection
  char buffer[64];
  int n;
  std::ostringstream ss;
  for(;;){
    bzero(buffer, sizeof(buffer));
    n = read(sock, buffer, sizeof(buffer) -1);
    if(n <= 0)
      break;
    buffer[n] = 0;
    ss << buffer;
  }

	// close the socket
	close(sock);

  std::string result = ss.str();
  if(result.find("\n\n") < result.find("\r\n\r\n"))
    result = result.substr(result.find("\n\n") + 2);
  else
    result = result.substr(result.find("\r\n\r\n") + 4);

  return result;
}

/*
int main(int argc, char **argv) {
	const char* server_name = "localhost";
  if(argc > 1)
    server_name = argv[1];


  Client client(server_name, "/info", 80);
  std::string result = client.request();
  printf("Received Total: '%s'\n\n", result.c_str());
  result = client.request();
  printf("Received Total: '%s'\n\n", result.c_str());
  result = client.request();
  printf("Received Total: '%s'\n\n", result.c_str());
	return 0;
}
*/