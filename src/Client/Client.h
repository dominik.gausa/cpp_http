#pragma once

#include <string>

class Client {
  std::string serverName;
  std::string url;
  int port;

public:
  Client(std::string serverName, std::string url, int port);

  std::string request();
};