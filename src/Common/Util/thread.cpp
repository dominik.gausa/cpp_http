#include "thread.h"

#ifndef USE_PTHREAD
#include <thread>

void Thread::Start(){
  thread = std::thread(&Thread::run, this);
  //thread.detach();
}

void *Thread::Stop(){
  thread.join();
  return this->result;
}

#else
#include <pthread.h>

void Thread::Start(){
  pthread_create(&this->pthread, NULL, delegator, this);
  //pthread_detach(this->pthread);
}

void *Thread::Stop(){
  pthread_join(this->pthread, NULL);
  return this->result;
}

void *delegator(void *ptr){
  ThreadPThread *srv = (ThreadPThread*)ptr;
  srv->run();
}

#endif

void Thread::setResult(void *res){
  this->result = res;
}