#pragma once

// #define USE_PTHREAD

#ifndef USE_PTHREAD
#include <thread>

#else

#include <pthread.h>

void *delegator(void *ptr);

#endif


class Thread {

  void *result;

#ifndef USE_PTHREAD
  std::thread thread;
#else
  pthread_t pthread;
#endif

public:
  virtual void run() = 0;

  void setResult(void *);

  void Start();
  void *Stop();
};
