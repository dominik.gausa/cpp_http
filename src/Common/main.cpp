/* The port number is passed as an argument */


#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <memory>
#include "Server/Server.h"
#include "Client/Client.h"

int main(int argc, char **argv)
{
  for(int i = 0; i < 10; i+=2){
    std::unique_ptr<Server> serv = std::make_unique<Server>(8080);

    Client client("localhost", "/info", 8080);
    std::string result = client.request();
    printf("Received Total: '%s'\n\n", result.c_str());

    printf("Waiting %5d\n", i);
    usleep(20*1000*1000);
  }
  return 0; 
}