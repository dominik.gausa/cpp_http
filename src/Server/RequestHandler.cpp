
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sstream>
#include <sys/types.h> 
#include <sys/socket.h>
#include <arpa/inet.h>




#include "RequestHandler.h"

static void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int RequestHandler::counter = 0;

RequestHandler::RequestHandler(int sockfd, std::shared_ptr<struct sockaddr_in> addr) :
  sockfd(sockfd), 
  cli_addr(addr) {
}

void RequestHandler::run() {
  printf("RequestHandler: got connection from %s port %d\n",
        inet_ntoa(cli_addr->sin_addr), ntohs(cli_addr->sin_port));

  char buffer[64];
  int n;
  do{
    bzero(buffer, sizeof(buffer));
    n = read(sockfd, buffer, sizeof(buffer));
    if (n < 0) error("ERROR reading from socket");
    printf("Read: %s\n", buffer);
  }while(n == sizeof(buffer));

  std::ostringstream ss;
  ss << "HTTP/1.1 200 OK" << "\r\n";
  ss << "Content-Length: 15" << "\r\n";
  ss << "\r\n";
  ss << "Hello from C++! " << counter++ << "!";
  std::string reply = ss.str();

  n = write(sockfd, reply.c_str(), reply.length());
  if (n < 0) error("ERROR writing to socket");

  printf("RequestHandler: handled %s port %d\n",
        inet_ntoa(cli_addr->sin_addr), ntohs(cli_addr->sin_port));
  close(sockfd);
}
