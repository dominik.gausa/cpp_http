#pragma once

#include "Util/thread.h"
#include <netinet/in.h>


class RequestHandler : public Thread {
  int sockfd;
  static int counter;
  std::shared_ptr<struct sockaddr_in> cli_addr;

public:
  RequestHandler(int sockfd, std::shared_ptr<struct sockaddr_in> addr);
  void run();
};

