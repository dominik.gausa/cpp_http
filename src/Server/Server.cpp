#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "Server.h"

#include "RequestHandler.h"

static void error(const char *msg)
{
    perror(msg);
    exit(1);
}

Server::Server(int port){
  printf("Server Constructor\n");
  this->_port = port;
  this->_run = true;
  this->Start();
}

Server::~Server(){
  printf("Server Destructor\n");
  this->Stop();
  printf("Server Destructed\n");
}

void Server::run(){
  printf("Server Started!\n"); 
  struct sockaddr_in serv_addr;
  // create a socket
  // socket(int domain, int type, int protocol)
  sockfd =  socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) 
      error("ERROR opening socket");

    // clear address structure
  bzero((char *) &serv_addr, sizeof(serv_addr));

  /* setup the host_addr structure for use in bind call */
  // server byte order
  serv_addr.sin_family = AF_INET;  

  // automatically be filled with current host's IP address
  serv_addr.sin_addr.s_addr = INADDR_ANY;  

  // convert short integer value for port must be converted into network byte order
  serv_addr.sin_port = htons(this->_port);

  // bind(int fd, struct sockaddr *local_addr, socklen_t addr_length)
  // bind() passes file descriptor, the address structure, 
  // and the length of the address structure
  // This bind() call will bind  the socket to the current IP address on port, portno
  if (bind(sockfd, (struct sockaddr *) &serv_addr,
            sizeof(serv_addr)) < 0) 
            error("ERROR on binding");

  // This listen() call tells the socket to listen to the incoming connections.
  // The listen() function places all incoming connection into a backlog queue
  // until accept() call accepts the connection.
  // Here, we set the maximum size for the backlog queue to 5.
  listen(sockfd, 5);

  // The accept() call actually accepts an incoming connection
  while(this->_run){
    // This accept() function will write the connecting client's address info 
    // into the the address structure and the size of that structure is clilen.
    // The accept() returns a new socket file descriptor for the accepted connection.
    // So, the original socket file descriptor can continue to be used 
    // for accepting new connections while the new socker file descriptor is used for
    // communicating with the connected client.
    
    std::shared_ptr<struct sockaddr_in> cli_addr = std::make_shared<struct sockaddr_in>();
    //struct sockaddr_in cli_addr;
    socklen_t clilen = sizeof(struct sockaddr_in);

    int newsockfd = accept(sockfd, 
          (struct sockaddr *) cli_addr.get(), &clilen);

    if (newsockfd < 0)
          break;

    RequestHandler *rqh = new RequestHandler(newsockfd, cli_addr);
    rqh->Start();
    
  }

  printf("Server Closing\n");
  close(sockfd);
  printf("Server Closing done\n");
}

void Server::Start(){
  Thread::Start();
}

void Server::Stop(){
  printf("Server Stopping...\n");
  this->_run = false;
  shutdown(this->sockfd, SHUT_RDWR);
  Thread::Stop();
  printf("Server Stopping...done\n");
}
