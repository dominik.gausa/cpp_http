#pragma once




#include "Util/thread.h"
#include <atomic>
class Server : public Thread {
private:
  std::atomic<bool> _run;
  int _port;
  int sockfd;

public:
  Server(int port);
  ~Server();

  void Start();
  void Stop();

  void run();
};